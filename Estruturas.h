#ifndef ESTRUTURAS_H
#define ESTRUTURAS_H

#include <stdio.h>
#include <conio.h>
#include<string.h>
#include <stdlib.h>

#define TAM_LISTA 1000
#define TAM_INODE 5
#define TAM_DADO 10
#define TAM_ARQUIVO 200
#define TAM_DIR 10
#define TAM_NOME 10
#define TAM_PILHA 500

typedef struct PilhaEstatica{
    int Tam;
    int elem[TAM_PILHA];
}PI;

typedef struct Diretorio{
	char NomeArquivo[TAM_DIR][TAM_NOME];
	int PosMemoria[TAM_DIR];
	int Pos;
	int Tam;
}Diretorio;

typedef struct ListaBlocosLivres{
    int PosMemoria[TAM_DIR];
	int Pos;
	int Tam;
}BlocosLivres;

typedef struct Dados{
	char Dado[TAM_DADO];
}BDados;

typedef struct Arquivo{
	/*
	* 0 Diret�rio
	* 1 Dados
	*/
	char flag;
	BDados SDado[TAM_ARQUIVO];
	Diretorio Dir[TAM_ARQUIVO];
	int Tam;
	int Pos;
}Arquivo;

typedef struct inodeIndireto{
	int Alocacao[TAM_INODE];
}inodeIndireto;

typedef struct inodePrincipal{
	/*
	*Rev 11/04
	*/
	char Data[10];
	char Hora[5];
	char Permissao[10];
	int Tamanho;
	char Caminho[10];
	char NomedoUsuario[8];
	char NomedoGrupo[8];
	int CLinksFisicos;
	int AlocacaoDireta[TAM_INODE];
	int InodeSimplesIndireto;
	int InodeDuploIndireto;
	int InodeTriploIndireto;
}inodePrincipal;

typedef struct bloco{
	/*
	*-1 Vazia
	* 0 Dados
	* 1 INode
	*11 inodeSimplesIndireto
	*12 inodeDuploIndireto
	*13 inodeTriploIndireto
	* 2 Diret�rio
	* 3 BadBlock
	*/
	char flag;
	BDados B;
	inodePrincipal IP;
	inodeIndireto IS;
	inodeIndireto ID;
	inodeIndireto IT;
	Diretorio D;
}Bloco;

typedef struct SuperBloco{
    int pos;///Maximo de blocos permitidos 2147483647 2^(32-1);
    int Tam;
    Bloco B[TAM_LISTA];
}LE;

typedef struct Pilha{
    int Tam;
    char Nome[TAM_PILHA][20];
    char PosMemoria[TAM_PILHA];
}Pilha;


void inicializa_INode_Memoria(int Alocacao[]){
    int count = 0;
    while(count < TAM_INODE){
        Alocacao[count] = -1;
        count++;
    }
}

void inicializa_INode_Principal(inodePrincipal &IP){
    IP.InodeDuploIndireto = -1;
    IP.InodeSimplesIndireto = -1;
    IP.InodeTriploIndireto = -1;
    IP.Tamanho = 0;
    IP.CLinksFisicos = 0;
    inicializa_INode_Memoria(IP.AlocacaoDireta);
}

void inicializa_Diretorio(Diretorio &D){
    D.Pos = 0;
    D.Tam = 0;
    int count = 0;
    while(count < TAM_DIR){
        D.PosMemoria[count] = -1;
        count++;
    }
}

void inicializa_BDados(BDados &B){
    int count = 0;
    while(count < TAM_DADO){
        B.Dado[count] = -1;/////A MUDAR
        count++;
    }
}

void inicializa_Bloco(Bloco &B){
    inicializa_Diretorio(B.D);
    inicializa_BDados(B.B);
    inicializa_INode_Memoria(B.IS.Alocacao);
    inicializa_INode_Memoria(B.ID.Alocacao);
    inicializa_INode_Memoria(B.IT.Alocacao);
    inicializa_INode_Principal(B.IP);
    B.flag = -1;
}

void inicializa_Arquivo(Arquivo &A,char flag){
    int count = 0;
    while(count < TAM_ARQUIVO){
        inicializa_Diretorio(A.Dir[count]);
        inicializa_BDados(A.SDado[count]);
        count++;
    }
    A.flag = flag;
    A.Pos = 0;
    A.Tam = -1;
}

int busca_Pos_Vazia_Bloco(LE l){
    int i = 0;
    while(i<l.Tam&&l.B[i].flag != -1){
        i++;
    }
    return i<l.Tam ? i : -1;
}

void exibeBlocos(LE l){
    l.pos = 0;
    while(l.pos<l.Tam){
        printf("%d ",l.B[l.pos++].flag);
    }
}

void exibe_Arquivo(LE &l,int Pos){
    inodePrincipal ip = l.B[Pos].IP;
    printf("Tamanho: %d\n",ip.Tamanho);
    printf("Links Fisicos: %d\n",ip.CLinksFisicos);
    printf("Inode Indireto: %d\n",ip.InodeSimplesIndireto);
    printf("Inode Indireto Duplo: %d\n",ip.InodeDuploIndireto);
    printf("Inode Indireto Triplo: %d\n",ip.InodeTriploIndireto);
    printf("Alocacao Direta %d ,",ip.AlocacaoDireta[0]);
    printf("%d ,",ip.AlocacaoDireta[1]);
    printf("%d ,",ip.AlocacaoDireta[2]);
    printf("%d ,",ip.AlocacaoDireta[3]);
    printf("%d \n",ip.AlocacaoDireta[4]);
}

char busca_pos_Vazias_bloco(LE l,PI &p,int Quant){
    p.Tam = 0;
    p.elem[p.Tam] = 0;
    while(Quant>0&&p.elem[p.Tam] != -1){
        p.elem[++p.Tam] = busca_Pos_Vazia_Bloco(l);
        l.B[p.elem[p.Tam]].flag = 5;
        Quant--;
    }
    if(p.elem[p.Tam] != -1)
        return 1;
    return 0;
}

int insere_no_Bloco_Busca(LE l,Bloco b){///// Retorna a Posi��o em que foi Inserido -1 para nulo
    int Pos = busca_Pos_Vazia_Bloco(l);
    if(Pos != -1)
        l.B[Pos] = b;
    return Pos;
}

char insere_Inode_Principal(LE &l,int Pos){
    Bloco b;
    if(Pos != -1){
        b.flag = 1;
        l.B[Pos] = b;
        return 1;
    }
    return 0;
}

char insere_Inode(LE &l,int Pos,char flag){
    if(Pos != -1){
        l.B[Pos].flag = flag;
        return 1;
    }
    return 0;
}

char insere_Inode_Indireto_Simples(LE &l,int Pos){
    Bloco b;
    if(Pos != -1){
        b.flag = 11;
        inicializa_INode_Memoria(b.IS.Alocacao);
        l.B[Pos] = b;
        return 1;
    }
    return 0;
}

char insere_Inode_Indireto_Duplo(LE &l,int Pos){
    Bloco b;
    if(Pos != -1){
        b.flag = 12;
        inicializa_INode_Memoria(b.ID.Alocacao);
        l.B[Pos] = b;
        return 1;
    }
    return 0;
}

char insere_Inode_Indireto_Triplo(LE &l,int Pos){
    Bloco b;
    if(Pos != -1){
        b.flag = 13;
        inicializa_INode_Memoria(b.IT.Alocacao);
        l.B[Pos] = b;
        return 1;
    }
    return 0;
}

char insere_Dado(LE &l,char Dado[],int Pos){
    Bloco b;
    if(Pos != -1){
        b.flag = 0;
        strcpy(b.B.Dado , Dado);
        l.B[Pos] = b;
        return 1;
    }
    return 0;
}

char insere_Diretorio_Bloco(LE &l,Diretorio &D,int Pos){
	   l.B[Pos].D.PosMemoria[l.B[Pos].D.Pos] = D.PosMemoria[D.Pos];
	   strcpy(l.B[Pos].D.NomeArquivo[l.B[Pos].D.Pos++],D.NomeArquivo[D.Pos++]);
	   l.B[Pos].D.Tam++;
}

char insere_Diretorio(LE &l,Arquivo &A,int Pos,int &count){
	int i = 0;
	if(Pos != -1){
		l.B[Pos].flag = 2;
		while(i < A.Dir[A.Pos].Tam && l.B[Pos].D.Tam < 10){
			insere_Diretorio_Bloco(l,A.Dir[A.Pos],Pos);
			i++;
		}
		if(l.B[Pos].D.Tam < 10 && A.Pos < A.Tam){
			A.Pos++;
			insere_Diretorio(l,A,Pos,count);
		}else{
            if(A.Dir[A.Pos].Pos>=A.Dir[A.Pos].Tam){
                A.Pos++;
            }
            count++;
		}
	}
}

int conta_Blocos(char Dados[]){
    int i = 0;
    while(Dados[i]!= '\0'){
        i++;
    }
    return i;
}

int calcula_Blocos(Arquivo A){
    int i;
    if(A.flag != -1){
        i = A.Tam;

        if(i<5){
            return i;
        }else
        if(i<10){
            return i+1;/////um bloco de endere�amento simples indireto;
        }else
        if(i<35){
            return i+7;/////um bloco de endere�amento Duplo indireto;
        }else
        if(i<175){
            return i+33;/////um bloco de endere�amento Triplo indireto;
        }else{
            A.Tam -=175;
            return i+33+calcula_Blocos(A);/////erro;
        }
    }
    return 0;
}

char endereca_Inode_IndiretoSimples(PI &p,LE &l,Arquivo &A,int Pos,inodePrincipal &IP){
    int count = 0;
    while(count < 5&&A.Pos < A.Tam){
        if(l.B[Pos].IS.Alocacao[count] == -1){/////S� insere no ponteiro caso ele nao esteja ocupado ou se for diret�rio pois neste caso um diret�rio pode ter varias entradas;
			l.B[Pos].IS.Alocacao[count] = p.elem[p.Tam--];
			if(A.flag){
				/*
				* 1 dados
				* 0 diretorio
				*/
	            insere_Dado(l,A.SDado[A.Pos++].Dado,l.B[Pos].IS.Alocacao[count]);
	            count++;
        	}else{
				insere_Diretorio(l,A,l.B[Pos].IS.Alocacao[count],count);
			}
			IP.Tamanho++;
		}else{
			insere_Diretorio(l,A,l.B[Pos].IS.Alocacao[count],count);
		}
    }
}

char endereca_Inode_IndiretoDuplo(PI &p,LE &l,Arquivo &A,int Pos,inodePrincipal &IP){
    int count = 0;
    while(count < 5&&A.Pos < A.Tam){
    	if(l.B[Pos].ID.Alocacao[count] == -1){
    		l.B[Pos].ID.Alocacao[count] = p.elem[p.Tam--];
       		 insere_Inode_Indireto_Simples(l,l.B[Pos].ID.Alocacao[count]);
		}
        endereca_Inode_IndiretoSimples(p,l,A,l.B[Pos].ID.Alocacao[count],IP);
        count++;
    }
}
char endereca_Inode_Principal_Arquivo(PI &p,LE &l,Arquivo &A,int Pos,inodePrincipal &IPE);
char endereca_Inode_IndiretoTriplo(PI &p,LE &l,Arquivo &A,int Pos,inodePrincipal &IP){
    int count = 0;
    while(count < 4&&A.Pos < A.Tam){
    	if(l.B[Pos].IT.Alocacao[count] == -1){
    		l.B[Pos].IT.Alocacao[count] = p.elem[p.Tam--];
	        insere_Inode_Indireto_Duplo(l,l.B[Pos].IT.Alocacao[count]);
		}
        endereca_Inode_IndiretoDuplo(p,l,A,l.B[Pos].IT.Alocacao[count],IP);
        count++;
    }
    if(count >= 4){
    	if(l.B[Pos].IT.Alocacao[count] == -1){
    		l.B[Pos].IT.Alocacao[count] = p.elem[p.Tam--];
	        insere_Inode_Principal(l,l.B[Pos].IT.Alocacao[count]);
		}
		inodePrincipal iaux;
		iaux.Permissao[0] = -9;
        endereca_Inode_Principal_Arquivo(p,l,A,l.B[Pos].IT.Alocacao[count],iaux);
        IP.Tamanho+=iaux.Tamanho;
    }
}

char endereca_Inode_Principal_Arquivo(PI &p,LE &l,Arquivo &A,int Pos,inodePrincipal &IPE){
    inodePrincipal IP;
    int count = 0;
    if(IPE.Permissao[0] == -10){
    	IP = IPE;
	}else if(IPE.Permissao[0] == -9){
		inicializa_INode_Principal(IP);
		IP.Permissao[0] = -9;
	}
	else{
        inicializa_INode_Principal(IP);
	}
    if(A.flag != -9){/////Se o arquivo � nulo ent�o n�o h� oque inserir, o inode � criado sem blocos anexados;
        while(count < 5&&A.Pos < A.Tam){
        	if(IP.AlocacaoDireta[count] == -1){/////S� insere no ponteiro caso ele nao esteja ocupado ou se for diret�rio pois neste caso um diret�rio pode ter varias entradas;
				IP.AlocacaoDireta[count] = p.elem[p.Tam--];
				if(A.flag){
					/*
					* 1 dados
					* 0 diretorio
					*/
		            insere_Dado(l,A.SDado[A.Pos++].Dado,IP.AlocacaoDireta[count]);
					count++;
	        	}else{
					insere_Diretorio(l,A,IP.AlocacaoDireta[count],count);
				}
				IP.Tamanho++;
			}else{
				insere_Diretorio(l,A,IP.AlocacaoDireta[count],count);
			}
        }
        if(A.Pos < A.Tam){
        	if(IP.InodeSimplesIndireto == -1){/////Se ele n�o exite, ele � criado
        		IP.InodeSimplesIndireto = p.elem[p.Tam--];
            	insere_Inode_Indireto_Simples(l,IP.InodeSimplesIndireto);
			}
            endereca_Inode_IndiretoSimples(p,l,A,IP.InodeSimplesIndireto,IP);
            if(A.Pos < A.Tam){
            	if(IP.InodeDuploIndireto == -1){/////Se ele n�o exite, ele � criado
            		IP.InodeDuploIndireto = p.elem[p.Tam--];
                	insere_Inode_Indireto_Duplo(l,IP.InodeDuploIndireto);
				}
                endereca_Inode_IndiretoDuplo(p,l,A,IP.InodeDuploIndireto,IP);
                if(A.Pos < A.Tam){
                	if(IP.InodeTriploIndireto == -1){/////Se ele n�o exite, ele � criado
	                    IP.InodeTriploIndireto = p.elem[p.Tam--];
	                    insere_Inode_Indireto_Triplo(l,IP.InodeTriploIndireto);
	            	}
                    endereca_Inode_IndiretoTriplo(p,l,A,IP.InodeTriploIndireto,IP);
                }
            }
        }
    }
    if(A.flag == 1){
        l.B[Pos].flag = 1;
        IP.Permissao[0] = 8;/////Arquivo
    }else
    if(A.flag == 0 || A.flag == -9){
        l.B[Pos].flag = 1;/////mechido
        IP.Permissao[0] = 7;/////diretorio
    }
    l.B[Pos].IP = IP;
    IPE = IP;
}

char formatar_SBloco(LE &l,int Tam){
    l.pos = 0;
    l.Tam = Tam;
    int count = 0;
    while(count < Tam){
        inicializa_Bloco(l.B[l.pos++]);
        count++;
    }
}

char cria_Arquivo_Dados(Arquivo &A){
    char a = 0;
    char aux[11];
    int count = -1,i;
    while(a != 27){
        count++;
        a = getch();
        aux[count] = a;
        if(count > 10){
            i = 0;
            while(count > 0){
                A.SDado[A.Tam].Dado[i] = aux[i];
                count--;
                i++;
            }
            A.Tam++;
            /////aux = {0};
        }
    }
    if(count > 0){
        i = 0;
        while(count > 0){
            A.SDado[A.Tam].Dado[i] = aux[i];
            count--;
            i++;
        }
        A.SDado[A.Tam].Dado[i] = '\0';
        A.Tam++;
    }
}

void exibe_Arquivo_Blocos(LE l,int pos,int posi){
	inodePrincipal ip = l.B[pos].IP;
	int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
			printf("%d, ",ip.AlocacaoDireta[i]);
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
				printf("%d, ",l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]);
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
					printf("%d, ",l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]);
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
						printf("%d, ",l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]);
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            exibe_Arquivo_Blocos(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
}
void inicializa_Pilha(PI p){
}

int cria_Arquivo(LE &l,Arquivo A){
    PI p;
    inodePrincipal iaux;
    int Quant;
    /*
    *Preenche Dados Inode;
    */
    inicializa_INode_Principal(iaux);
    inicializa_Pilha(p);
    int Pos = busca_Pos_Vazia_Bloco(l);
    (l).B[Pos].flag = 5;
    if(Pos != -1){
        A.flag = 1;
        Quant = calcula_Blocos(A);
        if(busca_pos_Vazias_bloco(l,p,Quant)){/////� Possivel Criar o arquivo;
            iaux.Permissao[0] = -10;
            endereca_Inode_Principal_Arquivo(p,l,A,Pos,iaux);
        }
    }
    /////exibe_Arquivo_Blocos(l,Pos,0);
	return Pos;
}

int cria_Diretorio(LE &l,Arquivo A){
    PI p;
    int Quant;
    inodePrincipal iaux;
    /*
    *Preenche Dados Inode;
    */
    inicializa_INode_Principal(iaux);
    int Pos = busca_Pos_Vazia_Bloco(l);
    if(Pos != -1){
        Quant = calcula_Blocos(A);
        if(busca_pos_Vazias_bloco(l,p,Quant+1)){/////� Possivel Criar o arquivo;
            iaux.Permissao[0] = -10;
            endereca_Inode_Principal_Arquivo(p,l,A,Pos,iaux);
        }
    }
    return Pos;
}
void deleta_Arquivo_InodePrincipal(LE &l,int Pos,int Posi,PI &p);
void inserir_no_Diretorio(LE &l,Arquivo A,int Pos){
	PI p;
	int Quant;
	Quant = calcula_Blocos(A)+5;
    if(busca_pos_Vazias_bloco(l,p,Quant)){/////� Possivel Criar o arquivo;
        l.B[Pos].IP.Permissao[0] = -10;
        endereca_Inode_Principal_Arquivo(p,l,A,Pos,l.B[Pos].IP);
    }
}

void deleta_Pilha(LE &l,PI &p){
    while(p.Tam>-1){
        if(p.elem[p.Tam]!=-1)
            inicializa_Bloco(l.B[p.elem[p.Tam]]);
        p.Tam--;
    }
}

void deleta_Arquivo_Diretorio(LE &l,int Pos,PI &p){
    Bloco b = l.B[Pos];
    Diretorio d;

    if(b.flag == 2){
        d = b.D;
        d.Pos = 2;
        p.elem[++p.Tam] = Pos;
        while(d.Pos<=d.Tam){
            p.elem[++p.Tam] = d.PosMemoria[d.Pos];
            deleta_Arquivo_InodePrincipal(l,d.PosMemoria[d.Pos++],0,p);
        }
    }
}

void deleta_Arquivo_Dados(LE &l,int Pos,PI &p){
    Bloco b = l.B[Pos];
    if(b.flag == 0){
        /////inicializa_Bloco(l.B[Pos]);
        p.elem[++p.Tam] = Pos;
    }
}

void deleta_Arquivo_InodePrincipal(LE &l,int Pos,int Posi,PI &p){
    Bloco b = l.B[Pos];
    inodePrincipal ip;
    int j = 0, d ;
    if(b.flag == 1){
        p.elem[++p.Tam] = Pos;
        ip = b.IP;
        for(int i = 0;i<ip.Tamanho;i++){
            if(i<5){
                p.elem[++p.Tam]=ip.AlocacaoDireta[i];
                deleta_Arquivo_Diretorio(l,ip.AlocacaoDireta[i],p);
                deleta_Arquivo_Dados(l,ip.AlocacaoDireta[i],p);
            }else
            if(i<10){

                for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    p.elem[++p.Tam] = l.B[ip.InodeSimplesIndireto].IS.Alocacao[j];
                    deleta_Arquivo_Diretorio(l,ip.AlocacaoDireta[i],p);
                    deleta_Arquivo_Dados(l,ip.AlocacaoDireta[i],p);
                }
                i+=j;
            }else
            if(i<35){
                p.elem[++p.Tam] = ip.InodeDuploIndireto;
                for(int k=0;k<5&&i+j<ip.Tamanho;k++){
                    p.elem[++p.Tam] = l.B[ip.InodeDuploIndireto].ID.Alocacao[k];
                    for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        p.elem[++p.Tam]=l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j];
                        deleta_Arquivo_Diretorio(l,ip.AlocacaoDireta[i],p);
                        deleta_Arquivo_Dados(l,ip.AlocacaoDireta[i],p);
                    }
                    i+=j;
                }
            }else
            if(i<135){
                p.elem[++p.Tam] = ip.InodeTriploIndireto;
                for(d = 0;d<4&&i+j<ip.Tamanho;d++){
                    p.elem[++p.Tam] = l.B[ip.InodeTriploIndireto].IT.Alocacao[d];
                    for(int k=0;k<5&&i+j<ip.Tamanho;k++){
                        p.elem[++p.Tam] = l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k];
                        for(j=0;j<5&&i+j<ip.Tamanho;j++){
                            p.elem[++p.Tam]=l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j];
                            deleta_Arquivo_Diretorio(l,ip.AlocacaoDireta[i],p);
                            deleta_Arquivo_Dados(l,ip.AlocacaoDireta[i],p);
                        }
                        i+=j;
                    }
                }
            }else
            if(d==4){
                p.elem[++p.Tam] = l.B[ip.InodeTriploIndireto].IT.Alocacao[4];
                deleta_Arquivo_InodePrincipal(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i,p);
                i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
                d++;
            }
        }
    }
}

int Busca_Diretorio_Max(LE l,int Pos,int posi){
	inodePrincipal ip = l.B[Pos].IP;
    Diretorio aux;
    int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
            aux = l.B[ip.AlocacaoDireta[i]].D;
           	if(i+1 == ip.Tamanho){
           		return ip.AlocacaoDireta[i];
			}
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
                aux = l.B[l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]].D;
                if(i+1 == ip.Tamanho){
	           		return l.B[ip.InodeSimplesIndireto].IS.Alocacao[j];
				}
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    aux = l.B[l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]].D;
					if(i+1 == ip.Tamanho){
		           		return l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j];
					}
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        aux = l.B[l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]].D;
                        if(i+1 == ip.Tamanho){
			           		return l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j];
						}
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            Busca_Diretorio_Max(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
	return -1;
}

char remove_Entrada_Diretorio(LE &l,int posD,int posDI,int PosE){
	inodePrincipal ip = l.B[posDI].IP;
	Diretorio dMax;
	Diretorio dMin = l.B[posDI].D;
	PosE++;
	int posMax = Busca_Diretorio_Max(l,posD,0);
	if(posMax == posDI && dMin.Tam-1 == PosE){
		dMin.Tam--;
		dMin.Pos--;
		l.B[posDI].D = dMin;
		return 1;
	}else
	if(posMax!=-1){
		dMax = l.B[posMax].D;
		strcpy(dMin.NomeArquivo[PosE],dMax.NomeArquivo[dMax.Tam-1]);
		dMin.PosMemoria[PosE] = dMax.PosMemoria[dMax.Tam-1];
		dMax.Pos--;
		dMax.Tam--;
        if(posMax == posDI){
            dMin.Pos--;
            dMin.Tam--;
            l.B[posMax].D = dMin;
        }else{
            l.B[posMax].D = dMax;
            l.B[posDI].D = dMin;
        }

		return 1;
	}
	return 0;
}

char deleta_Entrada_Diretorio(LE &l,int pos,int posi,char Nome[]){
	inodePrincipal ip = l.B[pos].IP;
    Diretorio aux;
    int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
            aux = l.B[ip.AlocacaoDireta[i]].D;
            aux.Pos = 0;
            while(aux.Pos<aux.Tam){
                if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
                	return remove_Entrada_Diretorio(l,pos,ip.AlocacaoDireta[i],aux.PosMemoria[aux.Pos]);
                aux.Pos++;
            }
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
                aux = l.B[l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]].D;
                aux.Pos = 0;
                while(aux.Pos<aux.Tam){
                    if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
	                	return remove_Entrada_Diretorio(l,pos,l.B[ip.InodeSimplesIndireto].IS.Alocacao[j],aux.PosMemoria[aux.Pos]);
	                aux.Pos++;
                }
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    aux = l.B[l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]].D;
                    aux.Pos = 0;
					while(aux.Pos<aux.Tam){
                        if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
		                	return remove_Entrada_Diretorio(l,pos,l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j],aux.PosMemoria[aux.Pos]);
		                aux.Pos++;
                    }
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        aux = l.B[l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]].D;
                        aux.Pos = 0;
                        while(aux.Pos<aux.Tam){
                            if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
			                	return remove_Entrada_Diretorio(l,pos,l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j],aux.PosMemoria[aux.Pos]);
			                aux.Pos++;
                        }
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            deleta_Entrada_Diretorio(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i,Nome);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
	return -1;
}


void deleta_Arquivo(LE &l,int Pos,int PosD,char Nome[]){
    PI p;
    p.Tam = -1;
    deleta_Entrada_Diretorio(l,PosD,0,Nome);
    deleta_Arquivo_Diretorio(l,Pos,p);
    deleta_Arquivo_Dados(l,Pos,p);
    deleta_Arquivo_InodePrincipal(l,Pos,0,p);
    deleta_Pilha(l,p);
}

void exibe_Diretorio(LE l,int Pos,int posi){
	inodePrincipal ip = l.B[Pos].IP;
	Bloco auxBloco;
    Diretorio aux;
    int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
            aux = l.B[ip.AlocacaoDireta[i]].D;
            aux.Pos = 0;
            while(aux.Pos<aux.Tam){
                auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                if(auxBloco.IP.Permissao[0]==8){
                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
                }else
				if(auxBloco.IP.Permissao[0]==7){
                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Diretorio");
                }
            }
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
                aux = l.B[l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]].D;
                aux.Pos = 0;
                while(aux.Pos<aux.Tam){
                    auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                    if(auxBloco.IP.Permissao[0]==8){
                   		printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
	                }else
					if(auxBloco.IP.Permissao[0]==7){
	                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Diretorio");
	                }
                }
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    aux = l.B[l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]].D;
                    aux.Pos = 0;
					while(aux.Pos<aux.Tam){
                        auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                        if(auxBloco.IP.Permissao[0]==8){
		                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
		                }else
						if(auxBloco.IP.Permissao[0]==7){
		                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Diretorio");
		                }
                    }
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        aux = l.B[l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]].D;
                        aux.Pos = 0;
                        while(aux.Pos<aux.Tam){
                            auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                            if(auxBloco.IP.Permissao[0]==8){
			                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
			                }else
							if(auxBloco.IP.Permissao[0]==7){
			                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Diretorio");
			                }
                        }
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            exibe_Arquivo_Blocos(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
}


int Busca_Diretorio(LE l,int Pos,int posi,char Nome[]){
	inodePrincipal ip = l.B[Pos].IP;
    Diretorio aux;
    int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
            aux = l.B[ip.AlocacaoDireta[i]].D;
            aux.Pos = 0;
            while(aux.Pos<aux.Tam){
                if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
                	return aux.PosMemoria[aux.Pos];
                aux.Pos++;
            }
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
                aux = l.B[l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]].D;
                aux.Pos = 0;
                while(aux.Pos<aux.Tam){
                    if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
	                	return aux.PosMemoria[aux.Pos];
	                aux.Pos++;
                }
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    aux = l.B[l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]].D;
                    aux.Pos = 0;
					while(aux.Pos<aux.Tam){
                        if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
		                	return aux.PosMemoria[aux.Pos];
		                aux.Pos++;
                    }
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        aux = l.B[l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]].D;
                        aux.Pos = 0;
                        while(aux.Pos<aux.Tam){
                            if(!stricmp(aux.NomeArquivo[aux.Pos],Nome))
			                	return aux.PosMemoria[aux.Pos];
			                aux.Pos++;
                        }
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            Busca_Diretorio(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i,Nome);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
	return -1;
}

void arvore(LE &l,int pos,int posi,int esp){
    inodePrincipal ip = l.B[pos].IP;
    Bloco auxBloco;
    Diretorio aux;
    int j = 0,d;
	for(int i = 0;i<ip.Tamanho;i++){
		if(i<5){
            aux = l.B[ip.AlocacaoDireta[i]].D;
            aux.Pos = 0;
            while(aux.Pos<aux.Tam){
                auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                if(auxBloco.IP.Permissao[0]==8){
                    int es = esp;
                    while(es>0){
                        es--;
                        printf("\t");
                    }
                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
                }else
				if(auxBloco.IP.Permissao[0]==7){
                    if(stricmp(aux.NomeArquivo[aux.Pos],".")!= 0&&stricmp(aux.NomeArquivo[aux.Pos],"..")!= 0){
                        int es = esp;
                        while(es>0){
                            es--;
                            printf("\t");
                        }
                        printf(" %s\n",aux.NomeArquivo[aux.Pos]);
                        arvore(l,aux.PosMemoria[aux.Pos++],posi,esp+1);
                    }
                    else{
                        aux.Pos++;
                    }
                }
            }
		}else
		if(i<10){
			for(j=0;j<5&&i+j<ip.Tamanho;j++){
                aux = l.B[l.B[ip.InodeSimplesIndireto].IS.Alocacao[j]].D;
                aux.Pos = 0;
                while(aux.Pos<aux.Tam){
                    auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                    if(auxBloco.IP.Permissao[0]==8){
                        int es = esp;
                        while(es>0){
                                es--;
                                printf("\t");
                        }
                   		printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
	                }else
					if(auxBloco.IP.Permissao[0]==7){
                        if(stricmp(aux.NomeArquivo[aux.Pos],".")!= 0&&stricmp(aux.NomeArquivo[aux.Pos],"..")!= 0){
                        int es = esp;
                        while(es>0){
                            es--;
                            printf("\t");
                        }
                        printf(" %s\n",aux.NomeArquivo[aux.Pos]);
                        arvore(l,aux.PosMemoria[aux.Pos++],posi,esp+1);
                    }else{
                            aux.Pos++;
                        }
                    }
                }
			}
			i+=j;
		}else
		if(i<35){
			for(int k=0;k<5&&i+j<ip.Tamanho;k++){
				for(j=0;j<5&&i+j<ip.Tamanho;j++){
                    aux = l.B[l.B[l.B[ip.InodeDuploIndireto].ID.Alocacao[k]].IS.Alocacao[j]].D;
                    aux.Pos = 0;
					while(aux.Pos<aux.Tam){
                        auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                        if(auxBloco.IP.Permissao[0]==8){
                            int es = esp;
                            while(es>0){
                                    es--;
                                    printf("\t");
                            }
		                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
		                }else
						if(auxBloco.IP.Permissao[0]==7){
                            if(stricmp(aux.NomeArquivo[aux.Pos],".")!= 0&&stricmp(aux.NomeArquivo[aux.Pos],"..")!= 0){
                                int es = esp;
                                while(es>0){
                                    es--;
                                    printf("\t");
                                }
                                printf(" %s\n",aux.NomeArquivo[aux.Pos]);
                                arvore(l,aux.PosMemoria[aux.Pos++],posi,esp+1);
                        }else{
                                aux.Pos++;
                            }
                        }
                    }
				}
				i+=j;
			}
		}else
		if(i<135){
			for(d = 0;d<4&&i+j<ip.Tamanho;d++){
				for(int k=0;k<5&&i+j<ip.Tamanho;k++){
					for(j=0;j<5&&i+j<ip.Tamanho;j++){
                        aux = l.B[l.B[l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[d]].ID.Alocacao[k]].IS.Alocacao[j]].D;
                        aux.Pos = 0;
                        while(aux.Pos<aux.Tam){
                            auxBloco = l.B[aux.PosMemoria[aux.Pos]];
                            if(auxBloco.IP.Permissao[0]==8){
                                int es = esp;
                                while(es>0){
                                        es--;
                                        printf("\t");
                                }
			                    printf(" %.10s %.10s\n",aux.NomeArquivo[aux.Pos++],"-Arquivo");
			                }else
							if(auxBloco.IP.Permissao[0]==7){
                                if(stricmp(aux.NomeArquivo[aux.Pos],".")!= 0&&stricmp(aux.NomeArquivo[aux.Pos],"..")!= 0){
                                    int es = esp;
                                    while(es>0){
                                        es--;
                                        printf("\t");
                                    }
                                    printf(" %s\n",aux.NomeArquivo[aux.Pos]);
                                    arvore(l,aux.PosMemoria[aux.Pos++],posi,esp+1);
                                }else{
                                    aux.Pos++;
                                }
                            }
                        }
					}
					i+=j;
				}
			}
		}else
		if(d==4){
            arvore(l,l.B[ip.InodeTriploIndireto].IT.Alocacao[4],i,esp+1);
            i+l.B[l.B[ip.InodeTriploIndireto].IT.Alocacao[4]].IP.Tamanho;
            d++;
        }
	}
}

void badblocks(LE l){
    l.pos = 0;
    int count = 0;
    while(l.pos<l.Tam){
        if(l.B[l.pos].flag == 3){
            printf("%d ,",l.pos);
            count++;
        }
        l.pos++;
    }
    printf("\nBadBlocks: %d \n",count);
}

void gerar_Arquivo(LE &l,int PosDir,char nome[],int TamBlocos){
    Arquivo A;
    Arquivo D;

    /////inicializa_Arquivo(A,1);
    inicializa_Arquivo(D,0);

    A.Tam = TamBlocos;


    D.Dir[0].PosMemoria[0] = cria_Arquivo(l,A);
    strcpy(D.Dir[0].NomeArquivo[0],nome);
    D.Tam = 1;
    D.Dir[0].Tam = 1;

    inserir_no_Diretorio(l,D,PosDir);
}


void gerar_Diretorio(LE &l,int PosDir,char nome[]){
    Arquivo A;
    Arquivo D;

    /////inicializa_Arquivo(A,1);
    inicializa_Arquivo(D,0);

    A.flag = -9;
    A.Tam = 1;

    D.Dir[0].PosMemoria[0] = cria_Diretorio(l,A);

    A.flag = 0;
    A.Dir[0].Tam = 2;
    A.Dir[0].Pos = 0;
    A.Dir[0].PosMemoria[0] = PosDir;
    strcpy(A.Dir[0].NomeArquivo[0],".");
    A.Dir[0].PosMemoria[1] = D.Dir[0].PosMemoria[0];
    strcpy(A.Dir[0].NomeArquivo[1],"..");
    inserir_no_Diretorio(l,A,D.Dir[0].PosMemoria[0]);

    strcpy(D.Dir[0].NomeArquivo[0],nome);
    D.Tam = 1;
    D.Dir[0].Tam = 1;

    inserir_no_Diretorio(l,D,PosDir);
}

void executa(){
	LE l;
    int count = 0;
    Arquivo A;
    A.flag = 1;
    while(count<50){
    	A.Dir[count].Tam = 2;
    	count++;
	}
    /*
    A.Tam = 0;
    while(count < 20){
        strcpy(A.SDado[count].Dado,"EU SOU A L");
        A.Tam++;
        count++;
    }
    strcpy(A.SDado[count].Dado,"ENDA");
    A.Tam++;
    A.Pos = 0;*/
    A.Tam = 250;
    formatar_SBloco(l,1000);
    cria_Arquivo(l,A);
    /////A.flag = 0;
    /////cria_Diretorio(l,A);
}
void inicializa_Disco(LE &l){
    Arquivo A;
    formatar_SBloco(l,1000);
    A.Tam = 1;
    A.flag = -9;
    cria_Diretorio(l,A);

    A.flag = 0;
    A.Pos = 0;
    A.Dir[0].Tam = 2;
    A.Dir[0].Pos = 0;
    A.Dir[0].PosMemoria[0] = 0;
    strcpy(A.Dir[0].NomeArquivo[0],".");
    A.Dir[0].PosMemoria[1] = 0;
    strcpy(A.Dir[0].NomeArquivo[1],"..");

    inserir_no_Diretorio(l,A,0);
}

int busca_Blocos_Livres(LE l){
    l.pos = 0;
    int count = 0;
    while(l.pos<l.Tam){
        if(l.B[l.pos].flag == -1){
            count++;
        }
        l.pos++;
    }
    return count;
}

void bloco_Bad(LE &l,int Pos){
    l.B[Pos].flag = -1;
}

void link_fisico(LE &l,int PosD,int PosA,char Nome[]){
    Arquivo A;
    A.Tam = 1;
    A.flag = 0;
    A.Pos = 0;
    A.Dir[0].Tam = 1;
    A.Dir[0].Pos = 0;
    A.Dir[0].PosMemoria[0] = PosA;
    strcpy(A.Dir[0].NomeArquivo[0],Nome);

    inserir_no_Diretorio(l,A,0);
}

#endif // ESTRUTURAS_H
