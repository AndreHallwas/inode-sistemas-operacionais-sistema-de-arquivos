/*
*Andr� Hallwas Ribeiro Alves
*/
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include "Estruturas.h"
#define TAM_STRING 10
#define TAM_Lista 50

typedef struct String{
	char info[TAM_Lista][TAM_STRING];
	int Tam;
	int Pos;
}string;

typedef struct Command{
	inodePrincipal dir;
	int posDir;
	string Comando;
	string Caminho;
	LE Disco;
	char Param;
}Cmd;
/*
*Defini��es do Prompt de Comandos do Sistema de Arquivos
*/
void _Loop(Command &Cmd);/*Base do Programa*/
char _Select(Command &Cmd);/*Seleciona o Comando*/
char _Broke(Command &Cmd);/*Quebra a String Para descobrir parametros e/ou Caminhos*/
char _Catch(Command &Cmd);/*Recebe a String*/
char _ls(Command &Cmd);/*Listar os nomes dos arquivos/diret�rio do arquivo de diret�rio, -l Listar os nomes dos arquivos/diret�rios com seus atributos*/
char _vi(Command &Cmd);/*Visualizar um arquivo regular. Se o arquivo na estiver corrompido apenas deve ser apresentado um msg dizendo que o arquivo foi visualizado. N�o � para abrir o arquivo realmente*/
char _chmod(Command &Cmd);/*Alterar as permiss�es de acesso a arquivos e diret�rios*/
char _mkdir(Command &Cmd);/*Criar diret�rios */
char _rmdir(Command &Cmd);/*Deletar diret�rios que estejam vazios*/
char _rm(Command &Cmd);/*Deletar arquivos*/
char _cd(Command &Cmd);/*Navegar nos diret�rios de forma absoluta ou relativa*/
char _link(Command &Cmd);/*-h criar link fisico , -s criar link simb�lico*/
char _unlink(Command &Cmd);/*-h remover link f�sico, -s remover link simb�lico*/
char _bad(Command &Cmd);/*Transformar um bloco em Bad*/
char _touch(Command &Cmd);/*Criar um arquivo Regular*/
char _df(Command &Cmd);/*Apresentar em bytes espa�o livre e ocupados do disco*/

char _ls(Command &Cmd){/*Listar os nomes dos arquivos/diret�rio do arquivo de diret�rio, -l Listar os nomes dos arquivos/diret�rios com seus atributos*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"ls")){
		if(aux.Pos<aux.Tam){
			return 2;
		}else{
			exibe_Diretorio(Cmd.Disco,Cmd.posDir,0);
			return 1;
		}
	}
	return 0;
}

char _vi(Command &Cmd){/*Visualizar um arquivo regular. Se o arquivo na estiver corrompido apenas deve ser apresentado um msg dizendo que o arquivo foi visualizado. N�o � para abrir o arquivo realmente*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"vi")){
        int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
        if(Pos!= -1){
            exibe_Arquivo(Cmd.Disco,Pos);
        }
        aux.Pos++;
        if(aux.Pos<aux.Tam){
                exibe_Arquivo_Blocos(Cmd.Disco,Pos,0);
                printf("\n");
        }
		return 1;
	}
	return 0;
}

char _chmod(Command &Cmd){/*Alterar as permiss�es de acesso a arquivos e diret�rios*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"chmod")){
		return 1;
	}
	return 0;
}

char _tree(Command &Cmd){
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"tree")){
        arvore(Cmd.Disco,Cmd.posDir,0,1);
		return 1;
	}
	return 0;
}

char _bd(Command &Cmd){
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"bd")){
        badblocks(Cmd.Disco);
		return 1;
	}
	return 0;
}

char _mkdir(Command &Cmd){/*Criar diret�rios */
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"mkdir")){
		if(Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]) == -1){
			gerar_Diretorio(Cmd.Disco,Cmd.posDir,aux.info[aux.Pos++]);
		}
		return 1;
	}
	return 0;
}

char _rmdir(Command &Cmd){/*Deletar diret�rios que estejam vazios*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"rmdir")){
        int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
		if(Pos != -1&&Cmd.Disco.B[Cmd.Disco.B[Pos].IP.AlocacaoDireta[0]].D.Tam<=2){
			deleta_Arquivo(Cmd.Disco,Pos,Cmd.posDir,aux.info[aux.Pos]);
		}
		return 1;
	}
	return 0;
}

char _rm(Command &Cmd){/*Deletar arquivos*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"rm")){
		int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
		if(Pos != -1){
			deleta_Arquivo(Cmd.Disco,Pos,Cmd.posDir,aux.info[aux.Pos]);
		}
		return 1;
	}
	return 0;
}

char _cd(Command &Cmd){/*Navegar nos diret�rios de forma absoluta ou relativa*/
	string aux = Cmd.Comando;
	Bloco auxBloco;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"cd")){
		if(aux.Pos<aux.Tam){
			int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
			if(Pos != -1){
				auxBloco = Cmd.Disco.B[Pos];
				if(auxBloco.IP.Permissao[0] == 7){
					Cmd.posDir = Pos;
					Cmd.dir = auxBloco.IP;
					if(!stricmp(aux.info[aux.Pos],".")){
						if(Cmd.Caminho.Tam>1){
							Cmd.Caminho.Tam--;
						}
					}else
					if(stricmp(aux.info[aux.Pos],"..") != 0){
						strcpy(Cmd.Caminho.info[Cmd.Caminho.Tam++],aux.info[aux.Pos]);
					}
				}
			}
		}
		return 1;
	}
	return 0;
}

char _link(Command &Cmd){/*-h criar link fisico , -s criar link simb�lico*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"link")){
		if(!stricmp(aux.info[aux.Pos++],"h")){
			int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos++]);
			if(Pos != -1){
                Cmd.Disco.B[Pos].IP.CLinksFisicos++;
				link_fisico(Cmd.Disco,Cmd.posDir,Pos,aux.info[aux.Pos])	;
			}
		}else{
            int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos++]);
			if(Pos != -1){
                Cmd.Disco.B[Pos].IP.CLinksFisicos++;
				link_fisico(Cmd.Disco,Cmd.posDir,Pos,aux.info[aux.Pos])	;
			}
		}
		return 1;
	}
	return 0;
}

char _unlink(Command &Cmd){/*-h remover link f�sico, -s remover link simb�lico*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"unlink")){
        if(!stricmp(aux.info[aux.Pos++],"h")){
			int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos++]);
			if(Pos != -1){
                Cmd.Disco.B[Pos].IP.CLinksFisicos--;
                deleta_Entrada_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
			}
		}else{
            int Pos = Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos++]);
			if(Pos != -1){
                Cmd.Disco.B[Pos].IP.CLinksFisicos--;
                deleta_Entrada_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]);
			}
		}
		return 1;
	}
	return 0;
}

char _bad(Command &Cmd){/*Transformar um bloco em Bad*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"bad")){
		int Pos = atoi(aux.info[aux.Pos++]);
		Cmd.Disco.B[Pos].flag = 3;
		return 1;
	}
	return 0;
}

char _touch(Command &Cmd){/*Criar um arquivo Regular*/
	string aux = Cmd.Comando;
	char Nome[TAM_STRING];
	int Blocos;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"touch")){
		if(Busca_Diretorio(Cmd.Disco,Cmd.posDir,0,aux.info[aux.Pos]) == -1){
			if(aux.Pos<aux.Tam){
				strcpy(Nome,aux.info[aux.Pos++]);
				if(aux.Pos<aux.Tam){
					Blocos = atoi(aux.info[aux.Pos++]);
					if(Blocos > 0){
						gerar_Arquivo(Cmd.Disco,Cmd.posDir,Nome,Blocos);
					}
				}
			}
		}
		return 1;
	}
	return 0;
}

char _df(Command &Cmd){/*Apresentar em bytes espa�o livre e ocupados do disco*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"df")){
        int Tam = busca_Blocos_Livres(Cmd.Disco);
        printf("%d - Blocos Livres\n%dmb -Espaco Livre em Disco\n",Tam,Tam/100);
        printf("%d - Blocos Perdidos \n",Tam/1000);
		return 1;
	}
	return 0;
}


char _rs(Command &Cmd){/*Apresentar em bytes espa�o livre e ocupados do disco*/
	string aux = Cmd.Comando;
	aux.Pos = 0;
	if(!stricmp(aux.info[aux.Pos++],"rs")){
        exibeBlocos(Cmd.Disco);
		return 1;
	}
	return 0;
}

void monta_Caminho(Command Cmd,char Caminho[]){
	string aux = Cmd.Caminho;
	aux.Pos = 0;
	if(aux.Tam){
		strcpy(Caminho,aux.info[aux.Pos++]);
		//strcat(Caminho,"/");
		while(aux.Pos<aux.Tam){
			strcat(Caminho,aux.info[aux.Pos++]);
			strcat(Caminho,"/");
		}
	}else{
		strcpy(Caminho,".");
	}

}

char _Says(const char param,Command Cmd){
	switch(param){
		case 1:
			char Caminho[TAM_STRING];
			monta_Caminho(Cmd,Caminho);
			printf("%s> ",Caminho);
		break;
		case 2:
			printf("Inode [Versao 0.0.1.9]");
			printf("\n2017 Unoeste - Ciencia da Computacao\n\n");
		break;

		case 12:
			printf("\nN�o foi possivel abrir o Caminho\n");
		break;

		case 98:
			printf("'%s' nao e reconhecido como um comando interno\nou externo, um programa operavel ou um arquivo em lotes.\n\n",Cmd.Comando.info[0]);
		break;
	}
	return 1;
}

char leave(Command &Cmd){/////Feito
	return !stricmp(Cmd.Comando.info[0],"exit");
}

char help(Command &Cmd){/////Feito
	return (!stricmp(Cmd.Comando.info[0],"help")) ? 1 : 0;
}

char _Catch(Command &Cmd){
	char aux[400];
	char *Com;
	gets(aux);
	Com = strtok(aux," ");
	if(Com!=NULL){
		strcpy(Cmd.Comando.info[Cmd.Comando.Tam++],Com);
		Com = strtok(NULL," -");
		while(Com!=NULL){
			strcpy(Cmd.Comando.info[Cmd.Comando.Tam++],Com);
			Com = strtok(NULL," ");
		}
		return 1;
	}
	return 0;

}


void init_String(string &st){
	st.Tam = 0;
	st.Pos = -1;
}

void init_Command(Command &Cmd){
	Cmd.Param = -1;
	init_String(Cmd.Comando);
	init_String(Cmd.Caminho);
	inicializa_Disco(Cmd.Disco);
	Cmd.dir = Cmd.Disco.B[0].IP;
	strcpy(Cmd.Caminho.info[0],"/");
	Cmd.Caminho.Tam++;
	Cmd.posDir = 0;
	/*
	strcpy(Cmd.Caminho.info[0],"dir");
	strcpy(Cmd.Caminho.info[1],"dir2");
	Cmd.Caminho.Tam = 2;
	*/
}

void _Loop(){
    Command Cmd;
    _Says(2,Cmd);
    init_Command(Cmd);
    while(!leave(Cmd)){
    	init_String(Cmd.Comando);
    	_Says(1,Cmd);
        if(_Catch(Cmd)){
        	if(_Select(Cmd)){
        		printf("\n");
			}else
				_Says(98,Cmd);
		}

    }
}

char _Select(Command &Cmd){
    return  _ls(Cmd)? 1:
            _vi(Cmd)? 1:
         _chmod(Cmd)? 1:
         _mkdir(Cmd)? 1:
         _rmdir(Cmd)? 1:
            _rm(Cmd)? 1:
            _cd(Cmd)? 1:
          _link(Cmd)? 1:
        _unlink(Cmd)? 1:
           _bad(Cmd)? 1:
         _touch(Cmd)? 1:
          leave(Cmd)? 1:
           help(Cmd)? 1:
            _rs(Cmd)? 1:
          _tree(Cmd)? 1:
            _bd(Cmd)? 1:
         _df(Cmd)? 1: 0;

}

int main(void){
	_Loop();
}
